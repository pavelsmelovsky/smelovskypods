Pod::Spec.new do |s|
  s.default_subspec  = "Core"
  s.name             = "PSUIKit"
  s.version          = "0.0.2"
  s.summary          = "My own simple UI kit"
  s.homepage         = "https://bitbucket.org/pavelsmelovsky/psuikit/overview"
  s.license          = 'MIT'
  s.author           = { "Pavel Smelovsky" => "pavel.smelovsky@gmail.com" }
  s.source           = { :git => "https://bitbucket.org/pavelsmelovsky/psuikit.git", :tag => "v#{s.version}" }
  s.social_media_url = 'https://twitter.com/PavelSmelovsky'

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.module_name   = 'PSUIKit'

  #s.resources = 'Pod/Assets/*'

  s.subspec 'Core' do |sp|
    sp.frameworks    = 'UIKit'

    sp.source_files  = 'Core/**/*.{swift,h,m}'
  end

  s.subspec 'FRC' do |sp|
    sp.dependency    'PSUIKit/Core'
    sp.source_files  = 'FRC/FRC/FRC/Source/**/*.swift'
  end

end
