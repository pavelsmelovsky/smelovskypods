Pod::Spec.new do |s|
  s.default_subspec  = "Core"
  s.name             = "PSFoundation"
  s.version          = "0.5.4"
  s.summary          = "My own simple foundation"
  s.homepage         = "https://bitbucket.org/pavelsmelovsky/psfoundation/overview"
  s.license          = 'MIT'
  s.author           = { "Pavel Smelovsky" => "pavel.smelovsky@gmail.com" }
  s.source           = { :git => "https://bitbucket.org/pavelsmelovsky/psfoundation.git", :tag => "v#{s.version}" }
  s.social_media_url = 'https://twitter.com/PavelSmelovsky'

  #s.platform     = :ios, '8.0'
  s.ios.deployment_target = '8.0'
  s.osx.deployment_target = '10.12'

  s.requires_arc = true

  s.module_name   = 'PSFoundation'

  #s.resources = 'Pod/Assets/*'

  s.subspec 'Core' do |sp|
    sp.frameworks    = 'Foundation'

    sp.source_files  = 'PSFoundation/Core/**/*.{swift,h,m}'
  end

  s.subspec 'ImageLoader' do |sp|
    sp.frameworks   = 'UIKit'
    sp.platform     = :ios, '8.0'

sp.source_files = 'PSFoundation/ImageLoader/**/*.{swift,h,m}'
  end

end
