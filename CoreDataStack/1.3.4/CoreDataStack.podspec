Pod::Spec.new do |s|
  s.default_subspec  = "Core"
  s.name             = "CoreDataStack"
  s.version          = "1.3.4"
  s.summary          = "Simple CoreData stack implementation"
  s.homepage         = "https://bitbucket.org/pavelsmelovsky/coredatastack/overview"
  s.license          = 'MIT'
  s.author           = { "Pavel Smelovsky" => "pavel.smelovsky@gmail.com" }
  s.source           = { :git => "https://bitbucket.org/pavelsmelovsky/coredatastack.git", :tag => "v#{s.version}" }
  s.social_media_url = 'https://twitter.com/PavelSmelovsky'

  s.pod_target_xcconfig = { "SWIFT_VERSION" => "4.1" }

  s.ios.deployment_target = '9.0'
  s.osx.deployment_target = '10.11'
  s.requires_arc = true

  s.module_name   = 'CoreDataStack'

  #s.resources = 'Pod/Assets/*'

  s.subspec 'Core' do |sp|
    sp.frameworks    = 'CoreData'

    sp.source_files  = 'CoreDataStack/CoreDataStack/Core/**/*.swift'
  end

  s.subspec 'Importer' do |sp|
    sp.dependency    'CoreDataStack/Core'
    sp.source_files  = 'CoreDataStack/CoreDataStack/Importer/**/*.swift'
  end

end
