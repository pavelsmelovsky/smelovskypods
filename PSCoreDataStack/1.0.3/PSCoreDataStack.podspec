Pod::Spec.new do |s|
  s.default_subspec  = "Core"
  s.name             = "PSCoreDataStack"
  s.version          = "1.0.3"
  s.summary          = "Simple CoreData stack implementation"
  s.homepage         = "https://bitbucket.org/pavelsmelovsky/coredatastack/overview"
  s.license          = 'MIT'
  s.author           = { "Pavel Smelovsky" => "pavel.smelovsky@gmail.com" }
  s.source           = { :git => "https://bitbucket.org/pavelsmelovsky/coredatastack.git", :tag => "v#{s.version}" }
  s.social_media_url = 'https://twitter.com/PavelSmelovsky'

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.module_name   = 'PSCoreDataStack'

  #s.resources = 'Pod/Assets/*'

  s.subspec 'Core' do |sp|
    sp.frameworks    = 'UIKit', 'CoreData'

    sp.source_files  = 'PSCoreDataStack/PSCoreDataStack/Core/**/*.swift'
  end

  s.subspec 'Importer' do |sp|
    sp.dependency    'PSCoreDataStack/Core'
    sp.source_files  = 'PSCoreDataStack/PSCoreDataStack/Importer/**/*.swift'
  end

end
